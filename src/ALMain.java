import java.util.Arrays;
import java.util.Scanner;
import java.lang.String;

public class ALMain {
	private static final int INITCAP = 10;
	private static int[] elements = new int[10];
	private static int size;
	private static Scanner choice;
	public static void main(String[] args) {
	
		int choiceB=0;
		boolean flag=true;
	
		while(flag) {
			System.out.print("Enter the desired operation: ");
			choice = new Scanner(System.in);
			String c= choice.nextLine();
			if(c.matches("[a-zA-Z]+")) {
				System.out.println("Usage: a b \n 1: add value b \n 2: remove"
						+ " all occurences of value b \n 3: print the size of the List");
			}
			else {
			String[] flags= c.split("");
			
			int choiceA= Integer.parseInt(flags[0]);
			if(flags.length==2) {
				choiceB= Integer.parseInt(flags[1]);
			}
		if (choiceA==1) 
			addValue(choiceB, elements);


		else if (choiceA==2) 
			removeOccurences(choiceB, elements);

		else if (choiceA==3) 
			System.out.println(size);

		else {
			flag=false;
		}
		}
		}
	}
	public static void addValue(int choice, int[] arr) {
		if(size>=arr.length)
			changeCapacity(INITCAP);
		elements[size]=choice;
		size++;
	}
	public static void changeCapacity(int capacityToGrow)
	{
		int oldCapacity = elements.length;
		int newCapacity = oldCapacity + capacityToGrow;
		elements = Arrays.copyOf(elements, newCapacity);
	}
	public static void removeOccurences(int element, int[] elements) {
		  int counter = 0;
	        for(int i=0;i<elements.length;i++)
	        {
	            if(elements[i]==element) 
	            {
	                elements[i]=0; 
	                int i1=0;
	                for(i1=i;i1<elements.length - 1;i1++)
	                {
	                    elements[i1]=elements[i1+1];
	                }
	                elements[i1]=0;
	                i--;
	                counter++;
	            }
	        }
	        size=size-counter; 
		
	}

}
